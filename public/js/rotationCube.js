function rotation(){
    const element = document.querySelector(".single-rb");

    if (element.classList.contains("animationRotation-right") === false
        && element.classList.contains("animationRotation-right2") === false
        && element.classList.contains("animationRotation-right3") === false
        && element.classList.contains("animationRotation-right4") === false
        && element.classList.contains("animationRotation-left") === false
        && element.classList.contains("animationRotation-left2") === false
        && element.classList.contains("animationRotation-left3") === false){
        document.querySelector('.single-rb').classList.remove('animationRotation-left4');
        document.querySelector('.single-rb').classList.add('animationRotation-right');


    }else if(element.classList.contains("animationRotation-right") === true || element.classList.contains("animationRotation-left3") === true){
        document.querySelector('.single-rb').classList.remove('animationRotation-right');
        document.querySelector('.single-rb').classList.remove('animationRotation-left3');
        document.querySelector('.single-rb').classList.add('animationRotation-right2');

    }else if(element.classList.contains("animationRotation-right2") === true || element.classList.contains("animationRotation-left2") === true){
        document.querySelector('.single-rb').classList.remove('animationRotation-right');
        document.querySelector('.single-rb').classList.remove('animationRotation-right2');
        document.querySelector('.single-rb').classList.remove('animationRotation-left2');
        document.querySelector('.single-rb').classList.add('animationRotation-right3');

    }else if (element.classList.contains("animationRotation-right3") === true || element.classList.contains("animationRotation-left") === true ){
      document.querySelector('.single-rb').classList.remove('animationRotation-right3');
      document.querySelector('.single-rb').classList.remove('animationRotation-left');
      document.querySelector('.single-rb').classList.add('animationRotation-right4');
    }else{
        document.querySelector('.single-rb').classList.remove('animationRotation-right4');
        document.querySelector('.single-rb').classList.add('animationRotation-right');
    }
}

function rotation2(){
    const element = document.querySelector(".single-rb");

    if (element.classList.contains("animationRotation-left") === false
        && element.classList.contains("animationRotation-left2") === false
        && element.classList.contains("animationRotation-left3") === false
        && element.classList.contains("animationRotation-left4") === false
        && element.classList.contains("animationRotation-right2") === false
        && element.classList.contains("animationRotation-right3") === false
        && element.classList.contains("animationRotation-right") === false){
        document.querySelector('.single-rb').classList.remove('animationRotation-right4');
        document.querySelector('.single-rb').classList.add('animationRotation-left');


    }else if(element.classList.contains("animationRotation-left") === true || element.classList.contains("animationRotation-right3") === true){
        document.querySelector('.single-rb').classList.remove('animationRotation-left');
        document.querySelector('.single-rb').classList.remove('animationRotation-right3');
        document.querySelector('.single-rb').classList.add('animationRotation-left2');


    }else if(element.classList.contains("animationRotation-left2") === true || element.classList.contains("animationRotation-right2") === true){

        document.querySelector('.single-rb').classList.remove('animationRotation-left');
        document.querySelector('.single-rb').classList.remove('animationRotation-left2');
        document.querySelector('.single-rb').classList.remove('animationRotation-right2');
        document.querySelector('.single-rb').classList.add('animationRotation-left3');

    }else if (element.classList.contains("animationRotation-left3") === true || element.classList.contains("animationRotation-right") === true){
        document.querySelector('.single-rb').classList.remove('animationRotation-left3');
        document.querySelector('.single-rb').classList.remove('animationRotation-right');
        document.querySelector('.single-rb').classList.add('animationRotation-left4');

    }else{
        document.querySelector('.single-rb').classList.remove('animationRotation-left4');
        document.querySelector('.single-rb').classList.add('animationRotation-left');
    }
}


