$(document).ready(function(){

    var item = document.getElementsByTagName('MAIN')[0];

    window.addEventListener('wheel', function(e) {

        if(window.innerWidth > 2030){
            if (e.deltaY > 0 ) item.scrollLeft += 1766;
            else item.scrollLeft -= 1766;

        }else if(window.innerWidth < 2029 && window.innerWidth >1920){
            if (e.deltaY > 0 ) item.scrollLeft += 1400;
            else item.scrollLeft -= 1400;

         }
        else if (window.innerWidth < 1921 && window.innerWidth >1710){
            if (e.deltaY > 0 ) item.scrollLeft += 1325;
            else item.scrollLeft -= 1325;
    }
        else if (window.innerWidth < 1711 ){
        if (e.deltaY > 0 ) item.scrollLeft += 1179;
        else item.scrollLeft -= 1179;
        }

    });

    // Add smooth scrolling to all links
    $("a").on('click', function(event) {

      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollRight: $(hash).offset().right
        }, 5000, function(){

          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        });
      } // End if
    });
  });
