        var navOpenBtn = document.querySelector('.hamburgeur');
        var nav = document.querySelector('.showNav');
        var navCloseBtn = document.querySelector('.close');
        var page = document.querySelector('#global');
        var wee = document.querySelector('.wee');

        //open nav
        navOpenBtn.addEventListener('click', function() {
            nav.classList.remove('invisible');
            nav.classList.remove('animeNav');
            wee.classList.remove('wee');
            navOpenBtn.classList.add('js-hidden');
            nav.classList.add('js-opened');
            nav.classList.add('visible');

        });

        //close nav
        navCloseBtn.addEventListener('click', function() {
            nav.classList.remove('visible');
            navOpenBtn.classList.remove('js-hidden');
            nav.classList.add('invisible');

        });

        //closing navigation if click outside it
        page.addEventListener('click', function(e) {

            var evTarget = e.target;

            if((evTarget !== nav) && (nav.classList.contains('js-opened')) && (evTarget !== navOpenBtn) && (evTarget.parentNode !== navOpenBtn)) {

                navOpenBtn.classList.remove('js-hidden');
                nav.classList.remove('visible');
                nav.classList.add('invisible');
            }

        });
