var modal = document.getElementById("myModal");
var modal2 = document.getElementById("myModal2");
var modal3 = document.getElementById("myModal3");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");
// var img2 = document.getElementById("myImg2");
// var img3 = document.getElementById("myImg3");
var img4 = document.getElementById("myImg4");
var captionText = document.getElementById("caption");

img.onclick = function(){
    modal.style.display = "block";
    captionText.innerHTML = this.alt;
};

// img2.onclick = function(){
//     modal3.style.display = "block";
//     captionText.innerHTML = this.alt;
// };

// img3.onclick = function(){
//     modal.style.display = "block";
//     captionText.innerHTML = this.alt;
// };

img4.onclick = function(){
    modal2.style.display = "block";
    captionText.innerHTML = this.alt;
};

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
var span2 = document.getElementsByClassName("close2")[0];
var span3 = document.getElementsByClassName("close3")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
};

span2.onclick = function() {
    modal2.style.display = "none";
};

span3.onclick = function() {
    modal3.style.display = "none";
};
//Slides dans le modal (Mdcarre)

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
    showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    var captionText = document.getElementById("caption");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " active";
    captionText.innerHTML = dots[slideIndex-1].alt;
}

// fin du slide dans le modal (mdcarre)



//Slides dans le modal (comingSoon)

var slideIndex2 = 1;
showSlides2(slideIndex2);

// Next/previous controls
function plusSlides2(z) {
    showSlides2(slideIndex2 += z);
}

// Thumbnail image controls
function currentSlide2(z) {
    showSlides2(slideIndex2 = z);
}


/* i = Y et n = z*/

function showSlides2(z) {
    var y;
    var slides2 = document.getElementsByClassName("mySlides2");
    var dots2 = document.getElementsByClassName("demo2");
    var captionText2 = document.getElementById("caption2");
    if (z > slides2.length) {slideIndex2 = 1}
    if (z < 1) {slideIndex2 = slides2.length}
    for (y = 0; y < slides2.length; y++) {
        slides2[y].style.display = "none";
    }
    for (y = 0; y < dots2.length; y++) {
        dots2[y].className = dots2[y].className.replace(" active2", "");
    }
    slides2[slideIndex2-1].style.display = "block";
    dots2[slideIndex2-1].className += " active2";
    captionText2.innerHTML = dots2[slideIndex2-1].alt;
}

// fin du slide dans le modal (comingSoon)

//Slides dans le modal (puissance4)

var slideIndex3 = 1;
showSlides3(slideIndex3);

// Next/previous controls
function plusSlides3(z) {
    showSlides3(slideIndex3 += z);
}

// Thumbnail image controls
function currentSlide3(z) {
    showSlides3(slideIndex3 = z);
}


/*  Y = t et  z =r*/

function showSlides3(r) {
    var t;
    var slides3 = document.getElementsByClassName("mySlides3");
    var dots3 = document.getElementsByClassName("demo3");
    var captionText3 = document.getElementById("caption3");
    if (r > slides3.length) {slideIndex3 = 1}
    if (r < 1) {slideIndex3 = slides3.length}
    for (t = 0; t < slides3.length; t++) {
        slides3[t].style.display = "none";
    }
    for (t = 0; t < dots3.length; t++) {
        dots3[t].className = dots3[t].className.replace(" active3", "");
    }
    slides3[slideIndex3-1].style.display = "block";
    dots3[slideIndex3-1].className += " active3";
    captionText3.innerHTML = dots3[slideIndex3-1].alt;
}

// fin du slide dans le modal (puissance4)
