<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Retrouvez ici mon cv en ligne ainsi que mon portfolio">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="google-site-verification" content="-637X_m1qgLMPYLTieL8OTAyo49vu0fFvDXo9V7SXY4" />

    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/reset.css') }}">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/images/favicone/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/images/favicone/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/images/favicone/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/images/favicone/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/images/favicone/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/images/favicone/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/images/favicone/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/images/favicone/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/images/favicone/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('/images/favicone/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/images/favicone/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/images/favicone/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/images/favicone/favicon-16x16.png') }}">
{{--    <link rel="manifest" href="/manifest.json">--}}
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <script src="https://kit.fontawesome.com/35d612b19a.js"></script>



    <title>Houba David || Curriculum vitae & Portfolio </title>
</head>
<body>



@include('layouts/partials/_nav')

<div id="global">
@yield('aPropos')

<div class="container animation">

<main id="frame">

    <div  id="other_box" class="other_box ">
        @yield('competences')
        @yield('experiences')
        @yield('personnalite')
        @yield('etudes')
        @yield('contact')
    </div>
    @include('layouts/partials/_footer')
</main>

</div>


</div>


<script src="{{asset('/js/cookiechoices.js')}}"></script><script>document.addEventListener('DOMContentLoaded', function(event){cookieChoices.showCookieConsentBar('Ce site utilise des cookies pour vous offrir le meilleur service. En poursuivant votre navigation, vous acceptez l’utilisation des cookies.', 'J’accepte', 'En savoir plus', 'http://davidhouba-dev.be/mentionsLegales');});</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
@include('flashy::message')
<script src="{{ asset('/js/menuGSM.js') }}"></script>
<script src="{{ asset('/js/smooth.js') }}"></script>
<script src="{{ asset('/js/mobileJs.js') }}"></script>
<script src="{{ asset('/js/magicMenu.js') }}"></script>
<script src="{{ asset('/js/modal.js') }}"></script>







</body>
</html>
