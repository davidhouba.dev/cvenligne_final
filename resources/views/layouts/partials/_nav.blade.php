<div class="hamburgeur">
    <i class="fas fa-bars"></i>
</div>

@if(app()->getLocale() == 'fr')
<nav id="menu" class="showNav animeNav">
    <ul class="nav">
        <li class="menu-item current-menu-item"><a  href="#competences" >{{ __('traduction.competences') }}</a>
        <div class="wee" style="left: 0; width: 81.7188px;"></div></li>
        <li class="menu-item"><a  href="#experiences" >{{ __('traduction.experiences') }}</a></li>
        <li class="menu-item"><a  href="#personnalite" >{{ __('traduction.personnalite') }}</a></li>
        <li class="menu-item"><a href="#etudes" >{{ __('traduction.etudes') }}</a></li>
        <li><a  href="#contact" >{{ __('traduction.contact') }}</a></li>
        <li ><a href="{{ asset('/download/HoubaDavid-CVPDF(français).pdf') }}"  target="_blank" >{{ __('traduction.telecharger') }} </a></li>

    </ul>
    <div class="close">
        <i class="fas fa-arrow-circle-left"></i>
    </div>
</nav>
@endif

@if(app()->getLocale() == 'en')

    <nav id="menu" class="showNav animeNav">
        <ul class="nav">
            <li class="menu-item current-menu-item"><a  href="#competences" >{{ __('traduction.competences') }}</a>
                <div class="wee" style="left: 0; width: 81.7188px;"></div></li>
            <li class="menu-item"><a  href="#experiences" >{{ __('traduction.experiences') }}</a></li>
            <li class="menu-item"><a  href="#personnalite" >{{ __('traduction.personnalite') }}</a></li>
            <li class="menu-item"><a href="#etudes" >{{ __('traduction.etudes') }}</a></li>
            <li><a  href="#contact" >{{ __('traduction.contact') }}</a></li>
            <li ><a id="changeLink" href="{{ asset('/download/HoubaDavid-CVPDF(Anglais).pdf') }}" target="_blank" >{{ __('traduction.telecharger') }} </a></li>
        </ul>
        <div class="close">
            <i class="fas fa-arrow-circle-left"></i>
        </div>
    </nav>

    @endif


<div class="traduction">

  <p><a href="{{ url('locale/fr') }}" > <img src="{{ asset('/images/icônes/france.svg') }}"  alt="icone france"></a></p>

    <p><a href="{{ url('locale/en') }}" ><img src="{{ asset('/images/icônes/united-kingdom.svg') }}"  alt="icone angleterre"></a></p>

</div>






