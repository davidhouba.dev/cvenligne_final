@component('mail::message')
# Hey Admin

    - {{$nom}}
    - {{$prenom}}
    - {{$email}}
    - {{$tel}}

@component('mail::panel')
{{$sujet}}
@endcomponent

Thanks,<br>
{{config('app.name')}}
@endcomponent

