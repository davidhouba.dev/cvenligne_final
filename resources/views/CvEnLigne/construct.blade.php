<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/construct.css') }}">
    <title>Portfolio</title>
</head>
<body>

<div id="wrapper">
    <img src="{{ asset('/images/travaux.jpg') }}" alt="">
    <p>Cette partie du site est encore en construction.</p>
    <p>Merci de repasser un peu plus tard.</p><br><br>

    <p>This page is under construction.</p>
    <p>Thanks to come back later.</p>

    <a class="myButton2" href="{{route('home')}}">{{ __('traduction.back') }}</a>

</div>


</body>
</html>
