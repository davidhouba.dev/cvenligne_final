@extends('layouts.home')

@section('aPropos')
   <div  id="aPropos" class="animationApropos">
       <h1>{{ __('traduction.developpeurWeb') }}</h1>
       <img src="{{ asset('/images/myAvatar(200x200).png') }}" alt="houbaDavid.png">
       <p>{{ __('traduction.Email') }} : <strong><a href="mailto:davidhouba.dev@gmail.com">davidhouba.dev@gmail.com</a></strong></p>
       <p><i class="fas fa-caret-right"></i>{{ __('traduction.habitation') }} </p>
       <p><i class="fas fa-caret-right"></i>{{ __('traduction.naissance') }}</p>
       <p><i class="fas fa-caret-right"></i>{{ __('traduction.permis') }}: A, B</p>
       <p><i class="fas fa-caret-right"></i><a href="{{route('portfolio')}}">{{ __('traduction.portfolio') }}</a></p>
       <p>{{ __('traduction.MaJ') }} : 03 / 01 / 2020</p>

   </div>

@stop

@section('competences')

    <div id="competences">
        <h2>{{ __('traduction.competences') }}</h2>

        <div class="competences">

        <h3>{{ __('traduction.language') }}</h3>
        <p><i class="fas fa-caret-right"></i><strong>PHP</strong>, <strong>SQL</strong>, <strong>Js</strong> (base) , <strong>Frameworks</strong>( <strong>Laravel</strong>, <strong>Codeigniter</strong>), <strong>HTML5</strong>/<strong>CSS3</strong>, <strong>Réact</strong>(base)</p>
        <h3>CMS</h3>
        <p><i class="fas fa-caret-right"></i><strong>Wordpress</strong>, <strong>Joomla</strong>, <strong>Prestashop</strong></p>
        <h3>{{ __('traduction.autres') }}</h3>
        <p><i class="fas fa-caret-right"></i><strong>Photoshop</strong>, <strong>Word</strong>,<strong> Excel</strong>, <strong>Boostrap</strong>, <strong>Gitkraken</strong>, <strong>Gitlab</strong>, Notions <strong>S.E.O.</strong></p>
        <h3>{{ __('traduction.titreLangues') }}</h3>
        <p><i class="fas fa-caret-right"></i>{{ __('traduction.langues') }}(B1)</p>
    </div>

</div>
@stop

@section('experiences')
    <div id="experiences">
        <h2>{{ __('traduction.experiences') }}</h2>

<div class="wrapper_experiences">
        <div class="tfe">
            <h3>{{ __('traduction.tfe') }}</h3>
        <div class="intitule">
            <p>{{ __('traduction.mdcarre') }}</p>
        </div>

        <div class="taches_effectuees">
        <p>{{ __('traduction.taches') }}</p>
            <ul>
                <li><i class="fas fa-caret-right"></i>{{ __('traduction.codeigniteur') }}</li>
                <li><i class="fas fa-caret-right"></i>{{ __('traduction.rechercheContactClient') }}</li>
                <li><i class="fas fa-caret-right"></i>{{ __('traduction.maquette') }}</li>
                <li><i class="fas fa-caret-right"></i>{{ __('traduction.dynamique') }} </li>
                <li><i class="fas fa-caret-right"></i>{{ __('traduction.administrateur') }}</li>
             </ul>
        </div>
        </div>

        <div class="stage">
            <h3>{{ __('traduction.stage') }}</h3>

            <div class="intitule">
                <p>{{ __('traduction.nomEntreprise') }} : <strong>WeLoveWeb</strong></p>
            </div>

            <div class="taches_effectuees">
                <p>{{ __('traduction.taches') }}</p>
                <ul>
                    <li><i class="fas fa-caret-right"></i>{{ __('traduction.wordpress') }}</li>
                    <li><i class="fas fa-caret-right"></i>{{ __('traduction.autresClients') }}</li>
                </ul>
            </div>
        </div>

    <div class="thales">
        <h3>{{ __('traduction.CQ') }}</h3>

        <div class="intitule">
            <p>{{ __('traduction.nomEntreprise') }} Thales Alenia Space Belgium</p>
        </div>

        <div class="taches_effectuees">
            <p>{{ __('traduction.taches') }}</p>
            <ul>
                <li><i class="fas fa-caret-right"></i>{{ __('traduction.assemblage') }}</li>
                <li><i class="fas fa-caret-right"></i>{{ __('traduction.chaineAssemblage') }}</li>
                <li><i class="fas fa-caret-right"></i>{{ __('traduction.moulage') }}</li>
            </ul>
        </div>
    </div>

    <div class="gardiennage">
        <h3>{{ __('traduction.gardiennage') }}</h3>

        <div class="intitule">
            <p>{{ __('traduction.nomEntreprise') }} Vigirisk</p>
        </div>

        <div class="taches_effectuees">
            <p>{{ __('traduction.taches') }}</p>
            <ul>
                <li><i class="fas fa-caret-right"></i>{{ __('traduction.survaillanceSite') }}</li>
                <li><i class="fas fa-caret-right"></i>{{ __('traduction.surveillanceMagasin') }}</li>
            </ul>
        </div>
    </div>

</div>
    </div>

@stop

@section('personnalite')
    <div id="personnalite">

        <h2>{{ __('traduction.personnalite') }}</h2>
<div class="wrapper_personnalite">
        <div class="qualite">

            <h3>{{ __('traduction.qualite') }}</h3>
            <p><i class="fas fa-caret-right"></i>{{ __('traduction.perseverent') }}</p>
            <p><i class="fas fa-caret-right"></i>{{ __('traduction.minutieux') }}</p>
            <p><i class="fas fa-caret-right"></i>{{ __('traduction.ALecoute') }}</p>
            <p><i class="fas fa-caret-right"></i>Sociable</p>
        </div>

        <div class="hobbies">

            <h3>Hobbies</h3>
            <p><i class="fas fa-caret-right"></i>{{ __('traduction.moto') }}</p>
            <p><i class="fas fa-caret-right"></i>{{ __('traduction.informatique') }}</p>
            <p><i class="fas fa-caret-right"></i>{{ __('traduction.jeuxvideos') }}</p>
            <p><i class="fas fa-caret-right"></i>{{ __('traduction.sortiesEntresAmis') }}</p>

        </div>
    </div>
</div>



@stop


@section('etudes')
    <div id="etudes">
        <h2>{{ __('traduction.etudes') }}</h2>

        <div id="liste">
            <p><i class="fas fa-caret-right"></i> {{ __('traduction.formationJs') }}</p>
            <p><i class="fas fa-caret-right"></i>{{ __('traduction.formationLaravel') }}</p>
            <p><i class="fas fa-caret-right"></i>2017-2019: Ifosup Wavre - bes({{ __('traduction.bes') }}) <strong>webdeveloper</strong></p>
            <p><i class="fas fa-caret-right"></i>2016-2017: {{ __('traduction.autodidacte') }}</p>
            <p><i class="fas fa-caret-right"></i>2009-2010: {{ __('traduction.ulb') }}</p>
            <p><i class="fas fa-caret-right"></i>2011-2012: {{ __('traduction.tobback') }}</p>
            <p><i class="fas fa-caret-right"></i>2010-2011: {{ __('traduction.psycho') }} - Marie haps</p>
            <p><i class="fas fa-caret-right"></i>2013-2014: {{ __('traduction.bachelier') }}</p>
        </div>
    </div>

@stop



@section('contact')
    <div id="contact">

        <h2>{{ __('traduction.contact') }}</h2>
        <div id="contact_wrapper">

            <form id="FormContact" action="{{route('store','#prenom')}}" method="post" novalidate>
                @csrf
                <label for="prenom">{{ __('traduction.prenom') }}</label>
                {!! $errors->first('prenom','<div class="is-invalid">:message</div>') !!}
                <input type="text" id="prenom" name="prenom" value="{{old('prenom')}}" placeholder="{{ __('traduction.prenom') }}" required>


                <label for="nom">{{ __('traduction.nom') }}</label>
                {!! $errors->first('nom','<div class="is-invalid">:message</div>') !!}
                <input type="text" id="nom" name="nom" placeholder="{{ __('traduction.nom') }}" value="{{old('nom')}}" required>


                <label for="email">Email</label>
                {!! $errors->first('email','<div class="is-invalid">:message</div>') !!}
                <input type="email" id="email" name="email" value="{{old('email')}}" placeholder="Email" required>


                <label for="tel">{{ __('traduction.telephone') }}</label>
                {!! $errors->first('tel','<div class="is-invalid">:message</div>') !!}
                <input type="tel" id="tel" name="tel" value="{{old('tel')}}" placeholder="{{ __('traduction.telephone') }}">

                <label for="sujet" id="center">{{ __('traduction.sujet') }}</label>

                <textarea id="sujet" name="sujet" placeholder="{{ __('traduction.texte') }}" style="height:200px">{{old('sujet')}}</textarea>
                {!! $errors->first('sujet','<div class="is-invalid">:message</div>') !!}
                <input type="submit" name='contactSubmit' value="{{ __('traduction.envoyer') }}">

            </form>



        </div>
    </div>

@stop


