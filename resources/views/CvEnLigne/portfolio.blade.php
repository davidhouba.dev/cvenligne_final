<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/portfolio.css') }}">

    <title>Portfolio</title>
</head>
<body>

<h2>{{ __('traduction.portfolio') }}</h2>


{{--Début modal --}}

<!-- The Modal -->

<!-- Début Modal MdCarre -->
<div id="myModal" class="modal">

    <!-- Container for the image gallery -->
    <div class="container">

        <!-- Full-width images with number text -->
        <div class="mySlides">
            <div class="numbertext">1 / 4</div>
            <img src="{{ asset('/images/MdCarre-menuAdmin.png') }}" >
        </div>

        <div class="mySlides">
            <div class="numbertext">2 / 4</div>
            <img src="{{ asset('/images/MdCarre-changeLog.png') }}" >
        </div>

        <div class="mySlides">
            <div class="numbertext">3 / 4</div>
            <img src="{{ asset('/images/MdCarre-changeSection.png') }}" >
        </div>

        <div class="mySlides">
            <div class="numbertext">4 / 4</div>
            <img src="{{ asset('/images/MdCarre-changeContact.png') }}" >
        </div>

        <!-- Next and previous buttons -->
        <a class="prev-modal" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next-modal" onclick="plusSlides(1)">&#10095;</a>

        <!-- Image text -->
        <div class="caption-container">
            <p id="caption"></p>
        </div>

        <!-- Thumbnail images -->
        <div class="row">
            <div class="column">
                <img class="demo cursor" src="{{ asset('/images/MdCarre-menuAdmin.png') }}" style="width:100%" onclick="currentSlide(1)" alt="{{ __('traduction.menuAdministrateur') }}">
            </div>
            <div class="column">
                <img class="demo cursor" src="{{ asset('/images/MdCarre-changeLog.png') }}" style="width:100%" onclick="currentSlide(2)" alt="{{ __('traduction.loginsAdministrateur') }}">
            </div>
            <div class="column">
                <img class="demo cursor" src="{{ asset('/images/MdCarre-changeSection.png') }}" style="width:100%" onclick="currentSlide(3)" alt="{{ __('traduction.sectionRealisations') }}">
            </div>
            <div class="column">
                <img class="demo cursor" src="{{ asset('/images/MdCarre-changeContact.png') }}" style="width:100%" onclick="currentSlide(4)" alt="{{ __('traduction.sectionContact') }}">
            </div>

        </div>
    </div>
    <!-- The Close Button -->
    <span class="close">&times;</span>

    <!-- Modal Caption (Image Text) -->
    <div id="caption"></div>
</div>
    <!-- fin modal(MdCarre)-->



    <!-- Début modal(comingSoon)-->

<div id="myModal2" class="modal2">
    <!-- Container for the image gallery -->
    <div class="container2">

        <!-- Full-width images with number text -->
        <div class="mySlides2">
            <div class="numbertext2">1 / 4</div>
            <img src="{{ asset('/images/ifosupAccueil.png') }}" >
        </div>

        <div class="mySlides2">
            <div class="numbertext2">2 / 4</div>
            <img src="{{ asset('/images/ifosupAdm.png') }}" >
        </div>

        <div class="mySlides2">
            <div class="numbertext2">3 / 4</div>
            <img src="{{ asset('/images/ifosupConn.png') }}" >
        </div>

        <div class="mySlides2">
            <div class="numbertext2">4 / 4</div>
            <img src="{{ asset('/images/ifosupModifPass.png') }}" >
        </div>

        <!-- Next and previous buttons -->
        <a class="prev-modal2" onclick="plusSlides2(-1)">&#10094;</a>
        <a class="next-modal2" onclick="plusSlides2(1)">&#10095;</a>

        <!-- Image text -->
        <div class="caption-container2">
            <p id="caption2"></p>
        </div>

        <!-- Thumbnail images -->
        <div class="row2">
            <div class="column2">
                <img class="demo2 cursor2" src="{{ asset('/images/ifosupAccueil.png') }}"  onclick="currentSlide2(1)" alt="{{ __('traduction.accueilSite') }}">
            </div>
            <div class="column2">
                <img class="demo2 cursor2" src="{{ asset('/images/ifosupAdm.png') }}"  onclick="currentSlide2(2)" alt="{{ __('traduction.menuAdministrateur') }}">
            </div>
            <div class="column2">
                <img class="demo2 cursor2" src="{{ asset('/images/ifosupConn.png') }}"  onclick="currentSlide2(3)" alt="{{ __('traduction.connectionAdministrateur') }}">
            </div>
            <div class="column2">
                <img class="demo2 cursor2" src="{{ asset('/images/ifosupModifPass.png') }}"  onclick="currentSlide2(4)" alt="{{ __('traduction.loginsAdministrateur') }}">
            </div>

        </div>
    </div>
    <!-- The Close Button -->
    <span class="close2">&times;</span>

    <!-- Modal Caption (Image Text) -->
    <div id="caption2"></div>
</div>



{{--fin modal comingsoon --}}



<!-- Début modal(puissance4)-->

<div id="myModal3" class="modal3">
    <!-- Container for the image gallery -->
    <div class="container3">

        <!-- Full-width images with number text -->
        <div class="mySlides3">
            <div class="numbertext3">1 / 4</div>
            <img src="{{ asset('/images/ComingSoon.jpg') }}" >
        </div>

        <div class="mySlides3">
            <div class="numbertext3">2 / 4</div>
            <img src="{{ asset('/images/ComingSoon.jpg') }}" >
        </div>

        <div class="mySlides3">
            <div class="numbertext3">3 / 4</div>
            <img src="{{ asset('/images/ComingSoon.jpg') }}" >
        </div>

        <div class="mySlides3">
            <div class="numbertext3">4 / 4</div>
            <img src="{{ asset('/images/ComingSoon.jpg') }}" >
        </div>

        <!-- Next and previous buttons -->
        <a class="prev-modal3" onclick="plusSlides3(-1)">&#10094;</a>
        <a class="next-modal3" onclick="plusSlides3(1)">&#10095;</a>

        <!-- Image text -->
        <div class="caption-container3">
            <p id="caption3"></p>
        </div>

        <!-- Thumbnail images -->
        <div class="row3">
            <div class="column3">
                <img class="demo3 cursor3" src="{{ asset('/images/ComingSoon.jpg') }}" style="width:100%" onclick="currentSlide3(1)" alt="ComingSoon">
            </div>
            <div class="column3">
                <img class="demo3 cursor3" src="{{ asset('/images/ComingSoon.jpg') }}" style="width:100%" onclick="currentSlide3(2)" alt="ComingSoon">
            </div>
            <div class="column3">
                <img class="demo3 cursor3" src="{{ asset('/images/ComingSoon.jpg') }}" style="width:100%" onclick="currentSlide3(3)" alt="ComingSoon">
            </div>
            <div class="column3">
                <img class="demo3 cursor3" src="{{ asset('/images/ComingSoon.jpg') }}" style="width:100%" onclick="currentSlide3(4)" alt="ComingSoon">
            </div>

        </div>
    </div>
    <!-- The Close Button -->
    <span class="close3">&times;</span>

    <!-- Modal Caption (Image Text) -->
    <div id="caption3"></div>
</div>

{{--fin modal puissance4 --}}


<div class="rotating-box">
    <div class="single-rb">
        <div class="front-side">
            <img id="myImg"  src="{{ asset('/images/md2-logo.jpg') }}" alt=" {{ __('traduction.mdCarre') }}">

        </div>

        <div class="back-side">
            <img id="myImg2"  src="{{ asset('/images/ComingSoon.jpg') }}" alt="ComingSoon">

        </div>

        <div class="left-side">
            <a target="_blank" href="https://codepen.io/DavidH356/pen/dyPpaVJ"><img  src="{{ asset('/images/Budgety.jpg') }}" alt="budgety"></a>

        </div>

        <div class="right-side">
            <img id="myImg4" src="{{ asset('/images/ifosup.jpg') }}" alt="{{ __('traduction.IFOSUP') }}">

        </div>
        <div class="top-side"></div>
        <div class="bottom-side"></div>

    </div>
</div>

<div class="next-left" onclick="rotation2()">
    <i class="fas fa-chevron-left"></i>
</div>

<div class="next-right" onclick="rotation()">
    <i class="fas fa-chevron-right"></i>
</div>

<a class="myButton2" href="{{route('home')}}">{{ __('traduction.back') }}</a>

<div class="footer">

    @include('layouts/partials/_footer')

</div>


<script src="https://kit.fontawesome.com/35d612b19a.js"></script>
<script src="{{ asset('/js/rotationCube.js') }}"></script>
<script src="{{ asset('/js/modal.js') }}"></script>
</body>
</html>
