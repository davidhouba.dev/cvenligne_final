<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Mail\ContactMessageCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use MercurySeries\Flashy\Flashy;

class HomeController extends Controller
{

    public function home(){
        return view('CvEnLigne.index');
    }

    public function store(ContactRequest $request){

        $mailable= new ContactMessageCreated($request->nom,$request->prenom,$request->email,$request->tel,$request->sujet);

        Mail::to('davidhouba.dev@gmail.com')->send($mailable);

        Flashy::message('Votre message à bien été envoyé ! Je vous recontacterai au plus vite !', 'http://your-awesome-link.com');


        return redirect()->home();

    }

    public function mentionsLegales(){
        return view('CvEnLigne.mentionsLegales');
    }

    public function construct(){
        return view('CvEnLigne.construct');
    }

    public function portfolio(){
        return view('CvEnLigne.portfolio');
    }

}
